#ifndef PWM_SERVER_DRIVER_H
#define PWM_SERVER_DRIVER_H

#include "i2c.h"
#include <memory>

namespace alexei_prog_snob {
class PwmServerDriver {
public:
    PwmServerDriver(int _bus, int _address);
    ~PwmServerDriver();
private:
    std::shared_ptr<I2C> m_i2cConnection;
};
}
#endif // PWM_SERVER_DRIVER_H