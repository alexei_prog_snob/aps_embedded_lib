#ifndef _I2C_H_
#define _I2C_H_


#include <inttypes.h>
#define BUFFER_SIZE (64)
namespace alexei_prog_snob {

class I2C {
public:
	/**
	 * @Description: connect to new i2c device
	 * @Param: _bus[in] - i2c bus number.
	 * @Param: _address[in] - i2c bus address.
	 */
	I2C(int _bus, int _address);
	~I2C();
	
	/**
	 * @Description: connect to i2c device if not connected
     * @Return: 0 on success, errno on fail.
	 */
	int ConnectToDevice();
	

	/**
	 * @Description: connect to new i2c device, if connected first we close and then connect
	 * @Param: _bus[in] - i2c bus number.
	 * @Param: _address[in] - i2c bus address.
     * @Return: 0 on success, errno on fail.
	 */
	int ConnectToDevice(int _bus, int _address);

	/**
	 * @Description: validate tht we connected currectlly.
	 * @Return: 0 on connection, other in is not connected
	 */ 
	int IsConnected();

	// Read/Write I2C
	int ReadByte(uint8_t _address, uint8_t* _retData);
	int WriteByte(uint8_t _address, uint8_t  _data);	

private:
	int m_busFd;
	int m_bus;
	int m_address;
	uint8_t m_readBuffer[BUFFER_SIZE];
	void CloseBus();
};


}

#endif //_I2C_H_
