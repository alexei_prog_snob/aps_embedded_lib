#include "i2c.h"

#include <sstream>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <errno.h>

using namespace alexei_prog_snob;

I2C::I2C(int _bus, int _address)
:m_busFd(-1),
 m_bus(_bus),
 m_address(_address) {
	ConnectToDevice();
}

I2C::~I2C() {

}

int I2C::IsConnected() {
	return m_busFd < 0 ? -1 : 0;
}

void I2C::CloseBus() {
	if (m_busFd >= 0) {
		close(m_busFd);
		m_busFd = -1;
	}
}

int I2C::ConnectToDevice(int _bus, int _address) {
	CloseBus();
	m_bus = _bus;
	m_address = _address;
	return ConnectToDevice();
}

int I2C::ConnectToDevice() {
	if (IsConnected() == 0) {
		return 0;
	}
	
	std::stringstream busFileName;
	busFileName << "/dev/i2c-" << m_bus; 
	m_busFd = open(busFileName.str().c_str(), O_RDWR);
	if (m_busFd < 0) {
		m_busFd = -1;
		return -EBADF;			
	}

	int retval = ioctl(m_busFd, I2C_SLAVE, m_address);
	if (retval < 0) {
		CloseBus();	
		return retval;	
	}

	return 0;
}


int I2C::ReadByte(uint8_t _address, uint8_t *_retData) {
	if (_retData == NULL) {
		return -1; // TODO: add errno
	}
	
	if (IsConnected() != 0) {
		return -1; // TODO: add errno
	}

	int retval = write(m_busFd, &_address, sizeof(_address));
	if (retval != sizeof(_address)) {
		return -1; //TODO: add errno	
	}

	retval = read(m_busFd, m_readBuffer, BUFFER_SIZE);

	if (retval != BUFFER_SIZE) {
		return -1; //TODO: add errno
	}

	*_retData = m_readBuffer[0];
	return 0;
}

int I2C::WriteByte(uint8_t _address, uint8_t  _data) {
	if (IsConnected() != 0) {
		return -1; // TODO: add errno
	}
	
	m_readBuffer[0] = _address;
	m_readBuffer[1] = _data;
	
	int retval = write(m_busFd, m_readBuffer, 2);
	if (retval != 2) {
		return -1; // TODO: add errno
	}
	return 0;
}






























